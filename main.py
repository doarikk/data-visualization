import io
import json
from fastapi import FastAPI, Response, BackgroundTasks, HTTPException, Request
from neo4j import GraphDatabase
from py2neo import Graph
import os
from dotenv import load_dotenv
from matplotlib import pyplot as plt
import pymongo
from pymongo import MongoClient
from pymongo.server_api import ServerApi
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from pydantic import BaseModel

load_dotenv()

uri=os.getenv("uri")
user=os.getenv("user")
pwd=os.getenv("pwd")
urimongo=os.getenv("urimongo")

print(uri,user,pwd)

templates = Jinja2Templates(directory="templates")

def connection():
    driver=GraphDatabase.driver(uri=uri, auth=(user,pwd))
    return (driver)

def create_img():
    plt.rcParams['figure.figsize'] = [7.50, 3.50]
    plt.rcParams['figure.autolayout'] = True
    fig = plt.figure() 
    
    query = """
    MATCH (u:Utilisateur)-[:Retweet]->()
    WITH u, count(*) AS retweetCount
    RETURN u.nom AS nomUtilisateur, retweetCount
    ORDER BY retweetCount DESC;
    """

    graph = Graph(uri, auth=(user, pwd))
    res = graph.query(query).to_data_frame()

    noms_utilisateurs = res['nomUtilisateur']
    nombre_retweets = res['retweetCount']

    plt.bar(noms_utilisateurs, nombre_retweets, color='skyblue')

    plt.title('Nombre de Retweets par Utilisateur')
    plt.xlabel('Utilisateur')
    plt.ylabel('Nombre de Retweets')

    # Affichage du plot
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png')
    plt.close(fig)
    return img_buf

def connectMongo():    
    # Create a new client and connect to the server
    client = MongoClient(urimongo, server_api=ServerApi('1'))
    # Send a ping to confirm a successful connection
    try:
        client.admin.command('ping')
        print("Pinged your deployment. You successfully connected to MongoDB!")
    except Exception as e:
        print(e)
    db = client["nosqlProject"]
    return (db)

app=FastAPI()
@app.get("/")
def default():
    return {"reponse":"you are in root"}

@app.post("/insertTweets")
async def default():
    db=connectMongo()
    collection = db["tweets"]
    try:
        with open("tweets.json", "r") as file:
            liste_tweets = json.load(file)
        result = collection.insert_many(liste_tweets)
        return {"message": f"{len(result.inserted_ids)} tweets insérés avec succès dans MongoDB."}
    except FileNotFoundError:
        raise HTTPException(status_code=404, detail="Le fichier JSON spécifié n'a pas été trouvé.")
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Une erreur s'est produite : {str(e)}")

# http://localhost:8081/getAllTweets?userId=1
# Récupération de la liste de tous les tweets de la base MongoDB
@app.get("/getAllTweets", response_class=HTMLResponse)
async def get_tweets(userId, request: Request):
    db=connectMongo()
    collection = db["tweets"]
    # Récupérer les tweets depuis la base de données MongoDB
    tweets = list(collection.find())
    # Rendre la page HTML en incluant les données des tweets
    return templates.TemplateResponse("index.html", {"request": request, "tweets": tweets})

# affiche les personnes qui ont retweeter le plus de tweet, classer par odre décroissant
@app.get("/img")
def get_img(background_tasks: BackgroundTasks):
    img_buf = create_img()
    background_tasks.add_task(img_buf.close)
    headers = {'Content-Disposition': 'inline; filename="out.png"'}
    return Response(img_buf.getvalue(), headers=headers, media_type='image/png')

#http://127.0.0.1:8081/count?label=Utilisateur
#Cela me renvoie la liste de tous les utilisateurs
@app.get("/count")
def countNodes(label):
    #with GraphDatabase.driver(uri, auth=(user,pwd)) as driver:
        #driver.verify_connectivity()
    driver_neo4j=connection()
    session=driver_neo4j.session()
    q1="""
        match(n) where labels(n) in [[$a]] return n.nom as Name
    """
    x={"a":label}
    results=session.run(q1, x)
    return {"reponse":[{"Name":row["Name"]} for row in results]}

class RetweetRequest(BaseModel):
    tweet_id: str
    user_id: str

# methode pour le retweet
@app.post("/retweet")
async def retweet(request: RetweetRequest):
    tweet_id = request.tweet_id
    user_id = request.user_id
    print(tweet_id, user_id)
    
    cypher_query = (
    """
    MATCH (user:Utilisateur {numUtilisateur: $num})
    CREATE (user)-[:Retweet ]->(tweet1:Tweet {idTweet: $a})
    """
    )
    x = {"a":tweet_id, "num": user_id}
    driver_neo4j=connection()
    with driver_neo4j.session() as session:
        result = session.run(cypher_query, x)
        if result.single():
            return {"message": "Retweet créé avec succès."}
        else:
            raise HTTPException(status_code=404, detail="Tweet non trouvé ou déjà retweeté")
